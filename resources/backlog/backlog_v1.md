# V1

- Entrer opérations
- Faire calculs d'opérations
- Afficher solde

| Back                                                         | Front                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| BDD :arrow_right: API REST<br />:arrow_right: Tableau d'opération | Formulaire :arrow_right: Ajouter une opération<br />Afficher chaque opérations<br />Afficher solde |

## Back : Doctrine

* Entités (repo) :arrow_right: OP :one:
* Controller :arrow_right: OP (method REST : GET(one), GET(all), POST(add), DELETE(delete)) 5️⃣

## Front : Angular

### Components

* Navbar :two:
* Footer (Licence, enformations légal) :one:
* Formulaire (OP) :five:
* Service (OP) :five:
* Entité (OP) :one:

### Template

* Template (*ngFor) :two:
* Template : affichage solde :one: