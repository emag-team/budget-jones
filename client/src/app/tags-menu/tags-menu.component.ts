import { Component, OnInit } from '@angular/core';
import { OperationService } from '../service/operation.service';
import { Operation } from '../entity/operation';

@Component({
  selector: 'app-tags-menu',
  templateUrl: './tags-menu.component.html',
  styleUrls: ['./tags-menu.component.css']
})
export class TagsMenuComponent implements OnInit {
  operations: Operation[];
  tags: any[] = [];
  tagsWait: any[] = [];
  total: number = 0;

  constructor(private service: OperationService) { }

  ngOnInit() {
    this.service.findAll().subscribe((data) => {
      this.operations = data;

      for (let x = 0; x < this.operations.length; x++) {
        for (let y = 0; y < this.operations[x].tags.length; y++) {
          this.tagsWait.push(this.operations[x].tags[y]);
        }
      }
      for (const item of this.tagsWait) {
        if (this.tags.indexOf(item)===-1) {
          this.tags.push(item);
        }
      }
    });
  }
}


