import { Component, OnInit } from '@angular/core';
import { Budget } from '../entity/budget';
import { BudgetService } from '../service/budget.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-budget-card',
  templateUrl: './budget-card.component.html',
  styleUrls: ['./budget-card.component.css']
})
export class BudgetCardComponent implements OnInit {
  id: number;
  budgetTotalInput: number = 0;
  budgetTotalOutput: number = 0;
  total: number = 0;
  budget: Budget = { name: "", sum: null };
  update: boolean = false;

  constructor(private service: BudgetService, route: ActivatedRoute, private router: Router) {
    route.params.subscribe(data => this.id = +data['id'])
  }

  ngOnInit() {
    this.service.findById(this.id).subscribe((response) => {
      this.budget = response;
      for (const operation of this.budget.operations) {
        if (operation.sum < 0) {
          this.budgetTotalOutput += operation.sum;
        } else {
          this.budgetTotalInput += operation.sum;
        }
      };
      this.total = this.budget.sum + this.budgetTotalInput + this.budgetTotalOutput;
    })
  }

  updateBudget() {
    this.service.update(this.budget).subscribe((data) => {
      this.budget = data;
      this.ngOnInit();
    })
  }

  deleteBudget() {
    
    this.service.delete(this.budget.id).subscribe(() => {
      this.router.navigate(["/budgets"]);
    })
  }
}
